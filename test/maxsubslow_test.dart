import 'dart:io';

void main() {
  var num = [6, -7, 6, 0, -1, -9, 4, -3, 8, -4];
  final inFile = File('6.in.txt').openWrite(mode: FileMode.append);
  inFile.write(num);
  inFile.close();
  int m = 0;
  for (int j = 1; j < num.length; j++) {
    for (int k = 1; k < num.length; k++) {
      int s = 0;
      for (int i = j; i < k; i++) {
        s = s + num[i];
        if (s > m) {
          m = s;
        }
      }
    }
  }
  print(m);
  final outFile = File('6.out.txt').openWrite(mode: FileMode.append);
  outFile.write(m);
  outFile.close();
}
