import 'dart:io';

void main(List<String> arguments) {
  var num = [];
  String? n = stdin.readLineSync()!;
  var lst = n.split(" ");
  for (int o = 0; o < 10; o++) {
    int x = int.parse(lst[o]);
    num.add(x);
  }
  int m = 0;
  for (int j = 1; j < num.length; j++) {
    for (int k = 1; k < num.length; k++) {
      int s = 0;
      for (int i = j; i < k; i++) {
        int l = num[i];
        s = s + l;
        if (s > m) {
          m = s;
        }
      }
    }
  }
  print(m);
}
